# Layout Builder Animate On Scroll

This module integrates layout builder with https://www.drupal.org/project/aos.

It allows to select an animation to be used on a layout builder block.
